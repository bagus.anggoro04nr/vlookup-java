package com.vlookup;

import java.awt.Dimension;

import com.vlookup.form.InputData;


public class Main {

    public static void main(String[] args) {
        InputData appGui = new InputData();
        appGui.setVisible(true);
        appGui.setEnabled(true);
        appGui.isPreferredSizeSet();
        appGui.setPreferredSize(new Dimension(450, 200));
        appGui.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        appGui.setMinimumSize(new Dimension(Integer.MIN_VALUE, Integer.MIN_VALUE));
        appGui.setSize(new Dimension(450, 200));
    }
}
