package com.vlookup.load;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;


public class Proses {

    private String pathJar, pathJar1, path, getData1;
    private File jarfile, jarfile1;
    private JarFile jar, jar1;
    private Manifest manifest, manifest1;
    private Attributes attrs, attrs1;
    private URL url, url1;
    private Class[] strClass;
    private ClassLoader cl, cl1;
    private Class mainClass, mainClass1;
    private Method mainMethod, mainMethod1;
    private String[] params, params1;
    private Object messageObj, messageObj1;
    private String col, row, filePath, filepath1, mainClassName, mainClassName1, getData;
    private Constructor constructor, constructor1;

    public Proses(String cols, String rows, String filePaths, String filepath1s){
        col = cols;
        row = rows;
        filePath = filePaths;
        filepath1 = filepath1s;
    }

	public void addData() throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        path = System.getProperty("user.dir");
        pathJar = path + "\\lib\\phpvlookup.jar";
        System.out.println(pathJar);
        jarfile = new File(pathJar);
        jar = new JarFile(jarfile);
        manifest = jar.getManifest();
        attrs = manifest.getMainAttributes();
        mainClassName = attrs.getValue("Main-Class");
        url = new URL("file", null, jarfile.getAbsolutePath());
        cl = new URLClassLoader(new URL[] {url});
        System.out.println(mainClassName);
        mainClass = cl.loadClass(mainClassName);
        strClass = new Class[]{String[].class};
        params1 = new String[] {filePath + "," + filepath1, col, row};
        mainMethod = mainClass.getDeclaredMethod("main", strClass);
        System.out.println(Arrays.toString(params1));
        constructor = mainClass.getConstructor();
        messageObj = constructor.newInstance();
        mainMethod.setAccessible(true);
        getData = mainMethod.invoke(messageObj, (Object) params1).toString();
        System.out.println(getData);
        if(getData == null) System.exit(0);
		
		pathJar1 = path + "\\lib\\createhtml.jar";
		jarfile1 = new File(pathJar1);
		jar1 = new JarFile(jarfile1);
		manifest1 = jar1.getManifest();
		attrs1 = manifest1.getMainAttributes();
		mainClassName1 = attrs1.getValue("Main-Class");
		url1 = new URL("file", null, jarfile1.getAbsolutePath());
		cl1 = new URLClassLoader(new URL[]{url1});
		mainClass1 = cl1.loadClass(mainClassName1);
		params1 = new String[]{getData};
		mainMethod1 = mainClass1.getMethod("main", strClass);
		System.out.println(Arrays.toString(params1));
        constructor1 = mainClass1.getConstructor();
		messageObj1 = constructor1.newInstance();
		mainMethod1.setAccessible(true);
        mainMethod1.invoke(messageObj1, (Object) params1);

        System.exit(0);

	}

}
