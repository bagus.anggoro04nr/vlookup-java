/*
 * Created by JFormDesigner on Fri Oct 18 20:12:27 ICT 2019
 */

package com.vlookup.form;


import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;


import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

import com.vlookup.load.Proses;


/**
 * @author unknown
 */
@SuppressWarnings("serial")
public class InputData extends JFrame {
	private JFileChooser jfc;
    private Proses prs;

	public InputData() {
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
		jfc = new JFileChooser();
		label1 = new JLabel();
		textField1 = new JTextField();
		label2 = new JLabel();
		textField2 = new JTextField();
		label3 = new JLabel();
		textField4 = new JTextField();
		label4 = new JLabel();
		textField5 = new JTextField();
		toggleButton1 = new JToggleButton();

		//======== this ========
		Container contentPane = getContentPane();
		contentPane.setLayout(new FormLayout(
			"6*(pref, 5px), center:pref:grow, 5px, pref",
			"6*(pref, 5px), pref, fill:pref"));

		//---- label1 ----
		label1.setText("Input Column");
		contentPane.add(label1, CC.xywh(3, 3, 2, 1));
		contentPane.add(textField1, CC.xywh(7, 3, 7, 1, CC.FILL, CC.FILL));

		//---- label2 ----
		label2.setText("Input Row In First Data");
		contentPane.add(label2, CC.xy(3, 5));
		contentPane.add(textField2, CC.xywh(7, 5, 7, 1, CC.FILL, CC.FILL));

		//---- label3 ----
		label3.setText("Select First Excel File");
		contentPane.add(label3, CC.xy(3, 7));
		contentPane.add(textField4, CC.xywh(7, 7, 7, 1, CC.FILL, CC.FILL));

		//---- label4 ----
		label4.setText("Select Second Excel File");
		contentPane.add(label4, CC.xy(3, 9));
		contentPane.add(textField5, CC.xywh(7, 9, 7, 1, CC.FILL, CC.FILL));

		//---- toggleButton1 ----
		toggleButton1.setText("Proses");
		contentPane.add(toggleButton1, CC.xywh(7, 14, 7, 1, CC.FILL, CC.FILL));
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization //GEN-END:initComponents

		txtFld5();
		txtFld4();
		btnProses();
	}


	// JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
	private JLabel label1;
	private JTextField textField1;
	private JLabel label2;
	private JTextField textField2;
	private JLabel label3;
	private JTextField textField4;
	private JLabel label4;
	private JTextField textField5;
	private JToggleButton toggleButton1;
	// JFormDesigner - End of variables declaration //GEN-END:variables

	private void txtFld4() {
		textField4.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// some stuff
				int returnValue = jfc.showOpenDialog(null);
				// int returnValue = jfc.showSaveDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
					System.out.println(selectedFile.getAbsolutePath());
					textField4.setText(selectedFile.getAbsolutePath());
				}

			}
		});
	}

	private void txtFld5() {
		textField5.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// some stuff
				int returnValue = jfc.showOpenDialog(null);
				// int returnValue = jfc.showSaveDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
					System.out.println(selectedFile.getAbsolutePath());
					textField5.setText(selectedFile.getAbsolutePath());
				}

			}
		});
	}

	private void btnProses() {

		toggleButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					prs = new Proses(textField1.getText().toUpperCase().trim(), textField2.getText().trim(), textField4.getText().trim(),
							textField5.getText().trim());
					prs.addData();
				} catch (NullPointerException | IOException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException ex) {
					ex.printStackTrace();
				}
			}
		});

	}

}
